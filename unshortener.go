package unshortener

import (
	"net/http"
)

// UnshortURL resolves shortened url and gives you the original one
func UnshortURL(url string) (unshortened string, err error) {

	resp, err := http.Head(url)

	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	unshortened = resp.Request.URL.String()
	return
}

// IsShortened returns true if the input url is a shortened one
func IsShortened(url string) (bool, error) {
	unshortened, err := UnshortURL(url)

	if err != nil {
		return false, err
	}

	if unshortened != url {
		return true, nil
	}
}
